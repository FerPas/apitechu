package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {

    static final String APISBaseUrl = "/apitechu/v1";

    @GetMapping(APISBaseUrl + "/products")
    public ArrayList<ProductModel> getProducts() {
        System.out.println("getProducts");

        return ApitechuApplication.productModels;
    }

    @GetMapping(APISBaseUrl + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id) {
        System.out.println("getProductById");
        System.out.println("La id del producto a obtener es " + id);

        ProductModel result = new ProductModel();

        for (ProductModel product : ApitechuApplication.productModels) {
            if (product.getId().equals(id)) {
                System.out.println("Producto encontrado");
                result = product;
            }
        }
        return result;
    }

    @PostMapping(APISBaseUrl + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct) {
        System.out.println("createProduct");
        System.out.println("la id del producto a crear es " +newProduct.getId());
        System.out.println("la desc del producto a crear es " +newProduct.getDesc());
        System.out.println("El precio del producto a crear es " +newProduct.getPrice());

        ApitechuApplication.productModels.add(newProduct);

        return newProduct;
    }

    @PutMapping(APISBaseUrl + "/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product,@PathVariable String id) {
        System.out.println("updateProduct");
        System.out.println("la id del producto a actualizar en parametro URL es" + id);
        System.out.println("la id del producto a actualizar es" + product.getId());
        System.out.println("la des del producto a actualizar es" + product.getDesc());
        System.out.println("el precio del producto a actualizar es" + product.getPrice());

        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());
            }
        }

        return product;
    }

    @PatchMapping(APISBaseUrl + "/products/{id}")
    public ProductModel updatePatchProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("updatePatchProduct");
        System.out.println("la id del producto a actualizar en parametro URL es" + id);
        System.out.println("la id del producto a actualizar es" + product.getId());
        System.out.println("la des del producto a actualizar es" + product.getDesc());
        System.out.println("el precio del producto a actualizar es" + product.getPrice());

        ProductModel patchProduct = new ProductModel();
        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                patchProduct.setId(productInList.getId());
                patchProduct.setPrice(productInList.getPrice());
                patchProduct.setDesc(productInList.getDesc());
                if (product.getPrice() != 0) {
                    productInList.setPrice(product.getPrice());
                    System.out.println("el precio del producto a patchear es" + product.getPrice());
                    patchProduct.setPrice(productInList.getPrice());
                    System.out.println("el precio del producto a patchear es" + patchProduct.getPrice());
                }
                if (product.getDesc() != null) {
                    productInList.setDesc(product.getDesc());
                    System.out.println("la des del producto a patchear es" + product.getDesc());
                    patchProduct.setDesc(productInList.getDesc());
                    System.out.println("la des del producto a patchear es" + patchProduct.getDesc());

                }
            }
        }

        return patchProduct;
        }
        @DeleteMapping(APISBaseUrl + "/products/{id}")
        public ProductModel deleteProduct(@PathVariable String id) {
            System.out.println("deleteProduct");
            System.out.println("la Id del producto a borrar es" + id);

            ProductModel result = new ProductModel();
            boolean foundCompany = false;

            for (ProductModel productInList : ApitechuApplication.productModels) {
                if (productInList.getId().equals(id)) {
                    System.out.println("producto encontrado");
                    foundCompany = true;
                    result = productInList;
                }
            }

            if (foundCompany == true) {
                System.out.println("borrando producto");
                ApitechuApplication.productModels.remove(result);
            }

            return result;
        }

    }


